package com.gynzo.data.exception;

public class ApiErrorException extends  Exception {
    public ApiErrorException() {
        super();
    }

    public ApiErrorException(final Throwable cause) {
        super(cause);
    }

    public ApiErrorException(final int errorCode) {
        super(new Throwable(Integer.toString(errorCode)));
    }

    public ApiErrorException(final String errorCodeName) {
        super(new Throwable(errorCodeName));
    }
}

