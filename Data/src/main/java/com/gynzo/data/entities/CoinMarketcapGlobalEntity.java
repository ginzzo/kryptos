package com.gynzo.data.entities;

import com.google.gson.annotations.SerializedName;

public class CoinMarketcapGlobalEntity {

    @SerializedName("active_cryptocurrencies")
    private int activeCryptocurrencies;

    @SerializedName("active_markets")
    private int activeMarkets;

    @SerializedName("bitcoin_percentage_of_market_cap")
    private long bitcoinPercentageOfMarketCap;

    @SerializedName("last_updated")
    private int lastUpdate;

    public int getActiveCryptocurrencies() {
        return activeCryptocurrencies;
    }

    public void setActiveCryptocurrencies(int activeCryptocurrencies) {
        this.activeCryptocurrencies = activeCryptocurrencies;
    }

    public int getActiveMarkets() {
        return activeMarkets;
    }

    public void setActiveMarkets(int activeMarkets) {
        this.activeMarkets = activeMarkets;
    }

    public long getBitcoinPercentageOfMarketCap() {
        return bitcoinPercentageOfMarketCap;
    }

    public void setBitcoinPercentageOfMarketCap(long bitcoinPercentageOfMarketCap) {
        this.bitcoinPercentageOfMarketCap = bitcoinPercentageOfMarketCap;
    }

    public int getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(int lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
