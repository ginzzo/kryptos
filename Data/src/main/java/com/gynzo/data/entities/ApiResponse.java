package com.gynzo.data.entities;

import com.google.gson.annotations.SerializedName;

public class ApiResponse<T> {

    @SerializedName("data")
    private T data;

    @SerializedName("metadata")
    private Metadata metadata;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public class Metadata {
        @SerializedName("timestamp")
        private int timestamp;

        @SerializedName("num_cryptocurrencies")
        private int numCryptocurrencies;

        @SerializedName("error")
        private String error;

        public int getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(int timestamp) {
            this.timestamp = timestamp;
        }

        public int getNumCryptocurrencies() {
            return numCryptocurrencies;
        }

        public void setNumCryptocurrencies(int numCryptocurrencies) {
            this.numCryptocurrencies = numCryptocurrencies;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }
    }

}
