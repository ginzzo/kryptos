package com.gynzo.data.repositories;

import com.gynzo.data.entities.TickerEntity;
import com.gynzo.data.mapper.TickerMapper;
import com.gynzo.data.store.ticker.RemoteTickerDataStore;
import com.gynzo.domain.models.Ticker;
import com.gynzo.domain.repositories.TickerRepository;
import com.gynzo.domain.usecases.ticker.GetTickersByFilter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class TickerDataRepository implements TickerRepository {
    private final RemoteTickerDataStore mRemoteTickerDataStore;
    private final TickerMapper mTickerMapper;

    @Inject
    public TickerDataRepository(RemoteTickerDataStore remoteTickerDataStore, TickerMapper tickerMapper) {
        this.mRemoteTickerDataStore = remoteTickerDataStore;
        this.mTickerMapper = tickerMapper;
    }

    @Override
    public Observable<List<Ticker>> getTickersByFilter(GetTickersByFilter.Params params) {
        return mRemoteTickerDataStore.getTickersByFilter(params)
                .toObservable()
                .map(new Function<List<TickerEntity>, List<Ticker>>() {
                    @Override
                    public List<Ticker> apply(List<TickerEntity> tickerEntityList) throws Exception {
                        return mTickerMapper.mapFromEntityList(tickerEntityList);
                    }
                });
    }

    @Override
    public Observable<List<Ticker>> getListingTickers() {
        return mRemoteTickerDataStore.getListingTickers()
                .toObservable()
                .map(new Function<List<TickerEntity>, List<Ticker>>() {
                    @Override
                    public List<Ticker> apply(List<TickerEntity> tickerEntityList) throws Exception {
                        return mTickerMapper.mapFromEntityList(tickerEntityList);
                    }
                });
    }

    @Override
    public Observable<Ticker> getTickerDetailsById(int tickerId) {
        return mRemoteTickerDataStore.getTickerDetailsById(tickerId)
                .toObservable()
                .map(new Function<TickerEntity, Ticker>() {
                    @Override
                    public Ticker apply(TickerEntity tickerEntity) throws Exception {
                        return mTickerMapper.mapFromEntity(tickerEntity);
                    }
                });
    }
}
