package com.gynzo.data.remote.services.ticker;

import com.gynzo.data.entities.ApiResponse;
import com.gynzo.data.entities.TickerEntity;

import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface TickerService {

    @GET("v2/listings/")
    Flowable<ApiResponse<List<TickerEntity>>> getListingTickers();

    @GET("v2/ticker/")
    Flowable<ApiResponse<List<TickerEntity>>> getTickersByFilter(
            //TODO
            /*@QueryMap Map<String, String> tickerFilter*/
    );

    @GET("v2/ticker/{ticker_id}")
    Flowable<ApiResponse<TickerEntity>> getTickerDetailsById(
            @Path("ticker_id") int tickerId
    );
}
