package com.gynzo.data.remote.services.coin_marketcap_global;

import com.gynzo.data.entities.ApiResponse;
import com.gynzo.data.entities.CoinMarketcapGlobalEntity;

import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface CoinMarketcapGlobalService {

    @GET("v2/global/")
    Flowable<ApiResponse<CoinMarketcapGlobalEntity>> getMarketGlobals(
            //TODO
            /*@QueryMap Map<String, String> currencyFilter*/
    );
}
