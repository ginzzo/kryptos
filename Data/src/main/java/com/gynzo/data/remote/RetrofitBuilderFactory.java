package com.gynzo.data.remote;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilderFactory {

    public static Retrofit makeRetrofitBuilder() {
        final OkHttpClient okHttpClient = makeOkHttpClient();
        return makeRetrofitBuilder(okHttpClient, new Gson());
    }

    private static Retrofit makeRetrofitBuilder(final OkHttpClient okHttpClient, final Gson gson) {
        return new Retrofit.Builder()
                .baseUrl("https://api.coinmarketcap.com/")
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    private static OkHttpClient makeOkHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .build();
    }

}
