package com.gynzo.data.mapper;

import java.util.Calendar;

public class MapperUtils {
    public static Calendar mapTimestampToCalendar(int timestamp) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        return calendar;
    }
}
