package com.gynzo.data.mapper;

import com.gynzo.data.entities.CoinMarketcapGlobalEntity;
import com.gynzo.domain.models.CoinMarketcapGlobal;

import javax.inject.Inject;

public class CoinMarketcapGlobalMapper {

    @Inject
    public CoinMarketcapGlobalMapper() {}

    public CoinMarketcapGlobal mapFromEntity(CoinMarketcapGlobalEntity coinMarketcapGlobalEntity) {
        return new CoinMarketcapGlobal(
                coinMarketcapGlobalEntity.getActiveCryptocurrencies(),
                coinMarketcapGlobalEntity.getActiveMarkets(),
                coinMarketcapGlobalEntity.getBitcoinPercentageOfMarketCap(),
                MapperUtils.mapTimestampToCalendar(coinMarketcapGlobalEntity.getLastUpdate())
        );
    }
}
