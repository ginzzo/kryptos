package com.gynzo.data.mapper;

import com.gynzo.data.entities.TickerEntity;
import com.gynzo.domain.models.Ticker;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class TickerMapper {

    @Inject
    public TickerMapper() {

    }

    public List<Ticker> mapFromEntityList(final List<TickerEntity> tickerEntityList) {
        final List<Ticker> tickers = new ArrayList<>();

        for (TickerEntity tickerEntity : tickerEntityList) {
            tickers.add(mapFromEntity(tickerEntity));
        }

        return tickers;
    }

    public Ticker mapFromEntity(final TickerEntity tickerEntity) {
        return new Ticker(
                tickerEntity.getId(),
                tickerEntity.getName(),
                tickerEntity.getSymbol(),
                tickerEntity.getWebsiteSlug(),
                tickerEntity.getRank(),
                tickerEntity.getCirculatingSupply(),
                tickerEntity.getMaxSupply(),
                MapperUtils.mapTimestampToCalendar(tickerEntity.getLastUpdate())
        );
    }
}
