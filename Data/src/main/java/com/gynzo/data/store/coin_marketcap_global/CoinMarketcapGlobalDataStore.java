package com.gynzo.data.store.coin_marketcap_global;

import com.gynzo.data.entities.CoinMarketcapGlobalEntity;

import io.reactivex.Flowable;

public interface CoinMarketcapGlobalDataStore {
    Flowable<CoinMarketcapGlobalEntity> getMarketGlobals();
}
