package com.gynzo.data.store.ticker;

import com.gynzo.data.entities.ApiResponse;
import com.gynzo.data.entities.TickerEntity;
import com.gynzo.domain.usecases.ticker.GetTickersByFilter;

import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;

public interface TickerDataStore {
    Flowable<List<TickerEntity>> getListingTickers();

    Flowable<List<TickerEntity>> getTickersByFilter(final GetTickersByFilter.Params tickerFilter);

    Flowable<TickerEntity> getTickerDetailsById(final int tickerId);
}
