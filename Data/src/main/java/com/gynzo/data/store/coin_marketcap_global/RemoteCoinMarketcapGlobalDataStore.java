package com.gynzo.data.store.coin_marketcap_global;

import com.gynzo.data.entities.ApiResponse;
import com.gynzo.data.entities.CoinMarketcapGlobalEntity;
import com.gynzo.data.exception.ApiErrorException;
import com.gynzo.data.remote.services.coin_marketcap_global.CoinMarketcapGlobalService;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;

public class RemoteCoinMarketcapGlobalDataStore implements CoinMarketcapGlobalDataStore {
    private final CoinMarketcapGlobalService mCoinMarketcapGlobalService;

    @Inject
    public RemoteCoinMarketcapGlobalDataStore(CoinMarketcapGlobalService coinMarketcapGlobalService) {
        this.mCoinMarketcapGlobalService = coinMarketcapGlobalService;
    }

    @Override
    public Flowable<CoinMarketcapGlobalEntity> getMarketGlobals() {
        return mCoinMarketcapGlobalService.getMarketGlobals()
                .map(new Function<ApiResponse<CoinMarketcapGlobalEntity>, CoinMarketcapGlobalEntity>() {
                    @Override
                    public CoinMarketcapGlobalEntity apply(ApiResponse<CoinMarketcapGlobalEntity> coinMarketcapGlobalEntityApiResponse) throws Exception {
                        if (coinMarketcapGlobalEntityApiResponse.getMetadata().getError() != null) {
                            throw new ApiErrorException(coinMarketcapGlobalEntityApiResponse.getMetadata().getError());
                        }
                        return coinMarketcapGlobalEntityApiResponse.getData();
                    }
                });
    }
}
