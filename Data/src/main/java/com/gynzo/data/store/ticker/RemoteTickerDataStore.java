package com.gynzo.data.store.ticker;

import com.gynzo.data.entities.ApiResponse;
import com.gynzo.data.entities.TickerEntity;
import com.gynzo.data.exception.ApiErrorException;
import com.gynzo.data.remote.services.ticker.TickerService;
import com.gynzo.domain.usecases.ticker.GetTickersByFilter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;

public class RemoteTickerDataStore implements TickerDataStore {
    private final TickerService mTickerService;

    @Inject
    public RemoteTickerDataStore(TickerService tickerService) {
        mTickerService = tickerService;
    }

    @Override
    public Flowable<List<TickerEntity>> getListingTickers() {
        return mTickerService.getListingTickers()
                .map(new Function<ApiResponse<List<TickerEntity>>, List<TickerEntity>>() {
                    @Override
                    public List<TickerEntity> apply(ApiResponse<List<TickerEntity>> listApiResponse) throws Exception {
                        if (listApiResponse.getMetadata().getError() != null) {
                            throw new ApiErrorException(listApiResponse.getMetadata().getError());
                        }

                        return listApiResponse.getData();
                    }
                });
    }

    @Override
    public Flowable<List<TickerEntity>> getTickersByFilter(GetTickersByFilter.Params tickerFilter) {
        return mTickerService.getTickersByFilter()
                .map(new Function<ApiResponse<List<TickerEntity>>, List<TickerEntity>>() {
                    @Override
                    public List<TickerEntity> apply(ApiResponse<List<TickerEntity>> listApiResponse) throws Exception {
                        if (listApiResponse.getMetadata().getError() != null) {
                            throw new ApiErrorException(listApiResponse.getMetadata().getError());
                        }

                        return listApiResponse.getData();
                    }
                });
    }

    @Override
    public Flowable<TickerEntity> getTickerDetailsById(int tickerId) {
        return mTickerService.getTickerDetailsById(tickerId)
                .map(new Function<ApiResponse<TickerEntity>, TickerEntity>() {
                    @Override
                    public TickerEntity apply(ApiResponse<TickerEntity> tickerEntityApiResponse) throws Exception {
                        if (tickerEntityApiResponse.getMetadata().getError() != null) {
                            throw new ApiErrorException(tickerEntityApiResponse.getMetadata().getError());
                        }

                        return tickerEntityApiResponse.getData();
                    }
                });
    }
}
