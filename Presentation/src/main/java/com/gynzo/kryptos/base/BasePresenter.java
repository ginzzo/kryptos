package com.gynzo.kryptos.base;

public interface BasePresenter<T> {
    void takeView(T view);

    void dropView();

}
