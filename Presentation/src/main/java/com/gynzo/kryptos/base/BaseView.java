package com.gynzo.kryptos.base;

public interface BaseView {
    void showToast(String text);
    void showToast(int text);
}
