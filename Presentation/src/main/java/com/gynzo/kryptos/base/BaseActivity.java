package com.gynzo.kryptos.base;

import android.widget.Toast;

import dagger.android.support.DaggerAppCompatActivity;

public class BaseActivity extends DaggerAppCompatActivity implements BaseView {
    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(int text) {
        Toast.makeText(this, this.getResources().getString(text), Toast.LENGTH_SHORT).show();
    }
}
