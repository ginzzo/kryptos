package com.gynzo.kryptos.tickers;

import android.os.Bundle;

import com.gynzo.kryptos.R;
import com.gynzo.kryptos.base.BaseActivity;
import com.gynzo.kryptos.injector.scopes.PerActivity;

import javax.inject.Inject;

public class TickersActivity extends BaseActivity implements TickersContract.View {

    @Inject
    TickersPresenter mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.takeView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
