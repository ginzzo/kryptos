package com.gynzo.kryptos.tickers;

import com.gynzo.kryptos.injector.scopes.PerActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class TickersModule {

    @PerActivity
    @Binds
    abstract TickersContract.Presenter tickersPresenter(TickersPresenter presenter);
}
