package com.gynzo.kryptos.tickers;

import com.gynzo.kryptos.base.BasePresenter;
import com.gynzo.kryptos.base.BaseView;

public interface TickersContract {

    public interface View extends BaseView {

    }

    public interface Presenter extends BasePresenter<View> {

    }

}
