package com.gynzo.kryptos.tickers;

import com.gynzo.domain.models.Ticker;
import com.gynzo.domain.usecases.ticker.GetTickersByFilter;
import com.gynzo.kryptos.injector.scopes.PerActivity;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

public class TickersPresenter implements TickersContract.Presenter {
    private TickersContract.View mTickersView;

    private final GetTickersByFilter mGetTickersByFilter;

    @Inject
    public TickersPresenter(GetTickersByFilter getTickersByFilter) {
        this.mGetTickersByFilter = getTickersByFilter;
    }

    @Override
    public void takeView(TickersContract.View view) {
        mTickersView = view;
        getTickers();
    }

    @Override
    public void dropView() {
        mTickersView = null;
    }

    private void getTickers() {
        mGetTickersByFilter.execute(
                new GetTickersByFilterSubscriber(),
                null
        );
    }

    private class GetTickersByFilterSubscriber extends DisposableObserver<List<Ticker>> {
        @Override
        public void onNext(List<Ticker> value) {
            if (value != null) {

            }
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {
            mGetTickersByFilter.dispose();
        }
    }
}
