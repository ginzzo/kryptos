package com.gynzo.kryptos;

import com.gynzo.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class UIThread extends PostExecutionThread {
    @Inject
    public UIThread() {
        super(AndroidSchedulers.mainThread());
    }
}
