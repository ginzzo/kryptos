package com.gynzo.kryptos.injector.modules;

import com.gynzo.data.remote.RetrofitBuilderFactory;
import com.gynzo.data.remote.services.coin_marketcap_global.CoinMarketcapGlobalService;
import com.gynzo.data.remote.services.ticker.TickerService;
import com.gynzo.data.repositories.CoinMarketcapGlobalDataRepository;
import com.gynzo.data.repositories.TickerDataRepository;
import com.gynzo.domain.repositories.CoinMarketcapGlobalRepository;
import com.gynzo.domain.repositories.TickerRepository;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class DataModule {

    @Provides
    TickerRepository tickerRepository(TickerDataRepository tickerDataRepository) {
        return tickerDataRepository;
    }

    @Provides
    CoinMarketcapGlobalRepository coinMarketcapGlobalRepository(
            CoinMarketcapGlobalDataRepository coinMarketcapGlobalDataRepository) {
        return coinMarketcapGlobalDataRepository;
    }


    @Provides
    @Singleton
    TickerService tickerService() {
        Retrofit retrofit = RetrofitBuilderFactory.makeRetrofitBuilder();
        return retrofit.create(TickerService.class);
    }

    @Provides
    @Singleton
    CoinMarketcapGlobalService coinMarketcapGlobalService() {
        Retrofit retrofit = RetrofitBuilderFactory.makeRetrofitBuilder();
        return retrofit.create(CoinMarketcapGlobalService.class);
    }
}
