package com.gynzo.kryptos.injector.modules;

import android.content.Context;

import com.gynzo.domain.executor.PostExecutionThread;
import com.gynzo.kryptos.UIThread;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjectionModule;
import dagger.android.DaggerApplication;
import io.reactivex.Scheduler;

@Module(includes = AndroidInjectionModule.class)
public abstract class ApplicationModule {

    @Binds
    abstract Context provideContext(DaggerApplication application);

    @Binds
    abstract PostExecutionThread providePostExecutionThread(UIThread uiThread);
}
