package com.gynzo.kryptos.injector.builder;

import com.gynzo.kryptos.injector.modules.ApplicationModule;
import com.gynzo.kryptos.tickers.TickersActivity;
import com.gynzo.kryptos.tickers.TickersModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = TickersModule.class)
    abstract TickersActivity bindTickersActivity();
}
