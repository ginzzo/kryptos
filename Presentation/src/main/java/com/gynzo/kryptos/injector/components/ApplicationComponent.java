package com.gynzo.kryptos.injector.components;

import com.gynzo.kryptos.KryptosApplication;
import com.gynzo.kryptos.injector.builder.ActivityBuilder;
import com.gynzo.kryptos.injector.modules.ApplicationModule;
import com.gynzo.kryptos.injector.modules.DataModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;


@Singleton
@Component (modules = {
        AndroidSupportInjectionModule.class,
        DataModule.class,
        ApplicationModule.class,
        ActivityBuilder.class})
public interface ApplicationComponent extends AndroidInjector<DaggerApplication> {

    void inject(KryptosApplication likiApplication);

    @Override
    void inject(DaggerApplication daggerApplication);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(DaggerApplication application);

        ApplicationComponent build();
    }

}
