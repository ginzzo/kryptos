package com.gynzo.kryptos;

import android.app.Application;

import com.gynzo.kryptos.injector.components.ApplicationComponent;
import com.gynzo.kryptos.injector.components.DaggerApplicationComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class KryptosApplication extends DaggerApplication {

    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent appComponent = DaggerApplicationComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }
}
