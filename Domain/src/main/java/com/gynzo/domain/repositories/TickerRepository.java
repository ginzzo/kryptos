package com.gynzo.domain.repositories;

import com.gynzo.domain.models.Ticker;
import com.gynzo.domain.usecases.ticker.GetTickersByFilter;

import java.util.List;

import io.reactivex.Observable;


public interface TickerRepository {

    Observable<List<Ticker>> getTickersByFilter(GetTickersByFilter.Params params);
    Observable<List<Ticker>> getListingTickers();
    Observable<Ticker> getTickerDetailsById(int tickerId);

}
