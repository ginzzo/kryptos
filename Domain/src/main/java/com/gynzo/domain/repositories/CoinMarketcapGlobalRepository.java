package com.gynzo.domain.repositories;

import com.gynzo.domain.models.CoinMarketcapGlobal;

import io.reactivex.Observable;

public interface CoinMarketcapGlobalRepository {
    Observable<CoinMarketcapGlobal> getCoinMarketcapData(final String currency);
}
