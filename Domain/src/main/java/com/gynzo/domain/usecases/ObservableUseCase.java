package com.gynzo.domain.usecases;

import com.gynzo.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public abstract class ObservableUseCase<T,Params> {
    private final CompositeDisposable mCompositeDisposable;

    @Inject
    PostExecutionThread mPostExecutionThread;

    public ObservableUseCase() {
        this.mCompositeDisposable = new CompositeDisposable();
    }

    protected abstract Observable<T> buildUseCaseObservable(Params params);

    public void execute(DisposableObserver<T> observer, Params params) {
        final Observable<T> observable = this.buildUseCaseObservable(params)
                .subscribeOn(Schedulers.io())
                .observeOn(mPostExecutionThread.scheduler);

        addDisposable(observable.subscribeWith(observer));
    }

    public void dispose() {
        if (!mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
    }

    private void addDisposable(Disposable disposable) {
        mCompositeDisposable.add(disposable);
    }
}
