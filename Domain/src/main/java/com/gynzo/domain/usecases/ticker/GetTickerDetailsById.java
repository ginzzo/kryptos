package com.gynzo.domain.usecases.ticker;

import com.gynzo.domain.models.Ticker;
import com.gynzo.domain.repositories.TickerRepository;
import com.gynzo.domain.usecases.ObservableUseCase;

import io.reactivex.Observable;

public class GetTickerDetailsById extends ObservableUseCase<Ticker, GetTickerDetailsById.Params> {
    private final TickerRepository mTickerRepository;

    public GetTickerDetailsById(TickerRepository tickerRepository) {
        this.mTickerRepository = tickerRepository;
    }

    @Override
    protected Observable<Ticker> buildUseCaseObservable(Params params) {
        if (params == null) throw new IllegalArgumentException("Must pass the Ticker id");
        return mTickerRepository.getTickerDetailsById(params.id);
    }

    public class Params {
        private int id;

        public Params(int id) {
            this.id = id;
        }

        public Params forId(final int id) {
            return new Params(id);
        }
    }
}
