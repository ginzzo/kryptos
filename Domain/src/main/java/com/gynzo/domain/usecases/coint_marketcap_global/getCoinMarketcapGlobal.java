package com.gynzo.domain.usecases.coint_marketcap_global;

import com.gynzo.domain.models.CoinMarketcapGlobal;
import com.gynzo.domain.repositories.CoinMarketcapGlobalRepository;
import com.gynzo.domain.usecases.ObservableUseCase;

import javax.inject.Inject;

import io.reactivex.Observable;

public class getCoinMarketcapGlobal extends ObservableUseCase<CoinMarketcapGlobal, getCoinMarketcapGlobal.Params> {
    private final CoinMarketcapGlobalRepository mCoinMarketcapGlobalRepository;

    @Inject
    public getCoinMarketcapGlobal(CoinMarketcapGlobalRepository coinMarketcapGlobalRepository) {
        this.mCoinMarketcapGlobalRepository = coinMarketcapGlobalRepository;
    }

    @Override
    protected Observable<CoinMarketcapGlobal> buildUseCaseObservable(Params params) {
        return mCoinMarketcapGlobalRepository.getCoinMarketcapData(params.currency);
    }

    public class Params {
        private String currency;

        public Params(String currency) {
            this.currency = currency;
        }

        public Params setCurrency(final String currency) {
            return new Params(currency);
        }
    }
}
