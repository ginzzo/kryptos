package com.gynzo.domain.usecases.ticker;

import com.gynzo.domain.models.Ticker;
import com.gynzo.domain.repositories.TickerRepository;
import com.gynzo.domain.usecases.ObservableUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetListingTickers extends ObservableUseCase<List<Ticker>, Object> {

    private TickerRepository mTickerRepository;

    @Inject
    public GetListingTickers(TickerRepository tickerRepository) {
        this.mTickerRepository = tickerRepository;
    }

    @Override
    protected Observable<List<Ticker>> buildUseCaseObservable(Object o) {
        return mTickerRepository.getListingTickers();
    }
}
