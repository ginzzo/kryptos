package com.gynzo.domain.usecases.ticker;

import com.gynzo.domain.models.Ticker;
import com.gynzo.domain.repositories.TickerRepository;
import com.gynzo.domain.usecases.ObservableUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;


public class GetTickersByFilter extends ObservableUseCase<List<Ticker>, GetTickersByFilter.Params> {

    private TickerRepository mTickerRepository;

    @Inject
    public GetTickersByFilter(TickerRepository tickerRepository) {
        this.mTickerRepository = tickerRepository;
    }

    @Override
    protected Observable<List<Ticker>> buildUseCaseObservable(Params params) {
        return mTickerRepository.getTickersByFilter(params);
    }

    public class Params {
        private int limit;
    }


}
