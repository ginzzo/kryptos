package com.gynzo.domain.models;

import java.util.Calendar;

public class CoinMarketcapGlobal {

    private int activeCryptocurrencies;
    private int activeMarkets;
    private long bitcoinPercentageOfMarketCap;
    private Calendar lastUpdate;

    public CoinMarketcapGlobal() {}

    public CoinMarketcapGlobal(int activeCryptocurrencies, int activeMarkets, long bitcoinPercentageOfMarketCap, Calendar lastUpdate) {
        this.activeCryptocurrencies = activeCryptocurrencies;
        this.activeMarkets = activeMarkets;
        this.bitcoinPercentageOfMarketCap = bitcoinPercentageOfMarketCap;
        this.lastUpdate = lastUpdate;
    }

    public int getActiveCryptocurrencies() {
        return activeCryptocurrencies;
    }

    public void setActiveCryptocurrencies(int activeCryptocurrencies) {
        this.activeCryptocurrencies = activeCryptocurrencies;
    }

    public int getActiveMarkets() {
        return activeMarkets;
    }

    public void setActiveMarkets(int activeMarkets) {
        this.activeMarkets = activeMarkets;
    }

    public long getBitcoinPercentageOfMarketCap() {
        return bitcoinPercentageOfMarketCap;
    }

    public void setBitcoinPercentageOfMarketCap(long bitcoinPercentageOfMarketCap) {
        this.bitcoinPercentageOfMarketCap = bitcoinPercentageOfMarketCap;
    }

    public Calendar getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Calendar lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
