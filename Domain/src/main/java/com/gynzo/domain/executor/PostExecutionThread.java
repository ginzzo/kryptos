package com.gynzo.domain.executor;

import javax.inject.Inject;

import io.reactivex.Scheduler;

public class PostExecutionThread {
    public Scheduler scheduler;

    @Inject
    public PostExecutionThread(Scheduler scheduler) {
        this.scheduler = scheduler;
    }
}
